## RPG Character creator

### Description
This java program was made to create RPG characters of different types:
**Warriors**, **Rangers** and **Mages**. The characters (aka heroes),
have different stats, depending on their type, level and equipment.
The stats include health points, strength, intelligence and dexterity.
 
---
#### Items
Heroes have four item slots, where they can equip weapons and armour.
There is room for one weapon, one head piece, one body piece and one
piece of leg armour. All items have a name and a level that scales
the item's bonus stats. The item level also works as a requirement for
the hero. This means that a hero cannot equip an item, if the item has
a higher level than the hero. (Hero at level 4, cannot equip level 5 item.)

---
The **weapons** have a base damage stat, which is increased if the weapon
type matches the hero type. This means that a melee weapon will give
a higher attack damage when wielded by a warrior, compared to for
example a mage.  
The weapon/hero-type combinations are:
- Melee weapon - Warrior
- Ranged weapon - Ranger
- Magic weapon - Mage

---
The **armour** gives the hero different bonus stats. These stats, and
their scaling, depend on the material the armour is made from.
A piece of armour can be made from Cloth, Leather or Plate.

The bonus stats are also scaled by what type of armour it is.
A piece of body armour gives 100% of the bonus stats, while
a piece of head armour gives 80%, and leg armour gives 60%.

---
### Making heroes, items and running methods
When making a hero, you need to specify the hero type as a parameter.
The hero type is chosen through the HeroType enum class, and makes
sure the right base stats are set.

**Creating a hero** (in this case a warrior), can be done by running the following in the main method:  
`Hero heroName = new Hero(HeroType.WARRIOR);`

---
To **create a weapon**, you need to specify the weapon type, name and level.
Creating a melee weapon:  
`Weapon weaponName = new Weapon(WeaponType.MELEE, "Weapon name", 1);`

---
To **create armour**, you also need to specify
the item slot. A piece of cloth head armour can be created as follows:  
`Armour armourName = new Armour(ArmourMaterial.CLOTH, ArmourSlot.HEAD, "Armour name", 1);`

---
All the methods you are supposed to use in the main method
return a string, so **method calls should be run with System.out.println**.  
The available methods are:  
`printStats()` - Prints the hero stats, including bonuses  
`gainXp(int xp)` - adds xp to hero's current experience points. Hero levels up if required xp is reached  
`equipArmour(Armour armour)` - Fills hero's armour slot with selected armour, adds bonus stats to hero base stats  
`equipWeapon(Weapon weapon)` - Fills hero's weapon slot, sets hero attack damage  
`printAttack()` - Returns a string that contains the attack power.

---
Project repo can be found at: [GitLab](https://gitlab.com/SarahMags/rpgheroes)
