package no.noroff.Heroes;
//Enums used in Hero.java to set base stats
public enum HeroType {
    WARRIOR,
    RANGER,
    MAGE
}
