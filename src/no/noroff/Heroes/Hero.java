package no.noroff.Heroes;

import no.noroff.Items.*;

public class Hero {
    protected int health;
    protected int strength;
    protected int dexterity;
    protected int intelligence;
    private final HeroType heroType;
    protected int level;
    private Weapon weapon;
    private Armour headArmour;
    private Armour bodyArmour;
    private Armour legsArmour;
    private int attackDmg;

    protected int experiencePoints; //Current XP
    protected double xpRequired = 100; //XP needed to level up. First requirement for level 2, is 100.

    public Hero(HeroType heroType){ //Creating hero at level 1, with zero xp and no weapon equipped. Stats set depending on hero type.
        this.heroType = heroType;
        level = 1;
        experiencePoints = 0;
        attackDmg = 0;

        switch (heroType){ //Depending on enum heroType, base stats are set.
            case WARRIOR:
                health = 150;
                strength = 10;
                dexterity = 3;
                intelligence = 1;
                break;
            case RANGER:
                health = 120;
                strength = 5;
                dexterity = 10;
                intelligence = 2;
                break;
            case MAGE:
                health = 100;
                strength = 2;
                dexterity = 3;
                intelligence = 10;
        }
    }

    public void levelUp() {
        switch (heroType){ //When leveling up, stats are scaled by the heroType
            case WARRIOR:
                health += 30;
                strength += 5;
                dexterity += 2;
                intelligence += 1;
                level++;
                break;
            case RANGER:
                health += 20;
                strength += 2;
                dexterity += 5;
                intelligence += 1;
                level++;
                break;
            case MAGE:
                health += 15;
                strength += 1;
                dexterity += 2;
                intelligence += 5;
                level++;
                break;
        }
    }

    public String gainXP(int addXP){ //Method adds xp, and checks if new total xp is enough to level up. Updates requiredXP for next level on level up.

        experiencePoints = experiencePoints + addXP; //Adds xp to existing xp points
        StringBuilder builder = new StringBuilder();

        if (experiencePoints >= xpRequired) { // xpRequired starts at 100 for level 2. Checks if hero has enough XP to level up.
            while (experiencePoints >= xpRequired) { //While loop to keep leveling up and updating xpRequired if given xp is enough to level up more than once
                levelUp();

                xpRequired = (int)(xpRequired + (100 * (Math.pow(1.1, level - 1))));
                /* Finds the new requirement by taking the requirement from the current level,
                and adding the increased required experience.
                The increased Required xp is found by multiplying 100 with 1.1 to the power of the previous level.
                Example: Finding the new requirement when the hero gets to level 3, we need to increase the initial requirement by 10% two times.
                100 * 1.1 * 1.1 = 121, or 100 * 1.1^(2) = 121. Since the hero keeps their xp when leveling up, we need to add this to the
                current required experience limit.
                I tried simplifying this, but it didn't work. If it works, don't touch it.*/
            }
            builder.append("Hero leveled up! Current level: " + level + ". XP: " + experiencePoints); //String set if hero levels up
        } else {
            int xpNeeded = (int) (xpRequired - experiencePoints); //Casting long xpRequired to int, rounds it down
            builder.append(addXP + " experience points added. Current XP: " + experiencePoints +
                    "\nXp to next level: " + xpNeeded); //If the user doesn't level up, the method returns a string with the current xp, and what they need to level up.
        }
        return builder.toString(); //Returning different strings depending on if the the required xp was reached or not
    }


    public String printAttack(){ //Returns string with attack damage
        StringBuilder builder = new StringBuilder();
        builder.append("Hero attacked! Attack damage: "+attackDmg); //When weapon gets equipped, attack damage is calculated.
        return builder.toString();
    }

    public String equipWeapon(Weapon weapon){
        StringBuilder builder = new StringBuilder();
        int weaponLevel = weapon.getWeaponLevel(); //Variable containing weapon level
        this.weapon = weapon;

        if (level >= weaponLevel) { //Check if hero level is higher than weapon level. Hero cannot equip item of higher level
            if (attackDmg != 0) { //Checks if the hero already has a weapon equipped. Resets attack damage if they do.
                attackDmg = 0;
            }
            switch (weapon.getWeaponType()) { //Calculates attack damage depending on the type of weapon and hero type.
                case MELEE:
                    if (heroType == HeroType.WARRIOR) { //Melee weapons give more damage when wielded by a warrior.
                        attackDmg = (int) (weapon.baseDmg + (strength * 1.5)); //Damage scaled by the warriors strength stat
                    } else {
                        attackDmg = weapon.baseDmg; //If hero type is not warrior, melee weapon keeps base damage with no bonus.
                    }
                    break;
                case RANGED:
                    if (heroType == HeroType.RANGER) { //Ranged weapons give more damage when wielded by a ranger.
                        attackDmg = (int) (weapon.baseDmg + (dexterity * 1.5)); //Damage scaled by the rangers dexterity stat
                    } else {
                        attackDmg = weapon.baseDmg; //If hero type is not ranger, ranged weapon keeps base damage with no bonus.
                    }
                    break;
                case MAGIC:
                    if (heroType == HeroType.MAGE) { //Magic weapons give more damage when wielded by a mage.
                        attackDmg = (int) (weapon.baseDmg + (intelligence * 1.5)); //Damage scaled by the mages intelligence stat
                    } else {
                        attackDmg = weapon.baseDmg; //If hero type is not mage, magic weapon keeps base damage with no bonus.
                    }
                    break;
            }
            builder.append("Weapon equipped");
        } else {
            builder.append("Hero level insufficient, reach level "+weaponLevel+" to equip armour");
        }
        return builder.toString();
    }

    public String equipArmour(Armour armour){
        int armourLevel = armour.getArmourLevel(); //Variable containing item level
        StringBuilder builder = new StringBuilder();
        if (level >= armourLevel) { //Check if hero level is higher than item level
            switch (armour.getArmourSlot()) { //Checks which type of armour, and sets the appropriate variable
                case HEAD:
                    if (headArmour != null) { //If hero is wearing head armour, unequip it.
                        unEquipArmour(headArmour);
                    }
                    headArmour = armour; //sets armour in head armour slot.
                    break;
                case BODY: //Equips body armour in body slot
                    if (bodyArmour != null) {
                        unEquipArmour(bodyArmour);
                    }
                    bodyArmour = armour;
                    break;
                case LEGS: //Equips legs armour in legs slot
                    if (legsArmour != null) {
                        unEquipArmour(legsArmour);
                    }
                    legsArmour = armour;
                    break;
            }

            health = health + armour.bonusHP; //bonus stats are calculated depending on armour slot when armour object is created
            strength = strength + armour.bonusStr;
            dexterity = dexterity + armour.bonusDex;
            intelligence = intelligence + armour.bonusInt;
            builder.append("Armour equipped");
        }else{
            builder.append("Hero level insufficient, reach level "+armourLevel+" to equip armour");
        }
        return builder.toString();
    }

    public void unEquipArmour(Armour armour){ //Unequipping armour subtracts the bonus stats that were added
        health = health - armour.bonusHP;
        strength = strength - armour.bonusStr;
        dexterity = dexterity - armour.bonusDex;
        intelligence = intelligence - armour.bonusInt;
    }

    public String printStats(){
        StringBuilder builder = new StringBuilder(); //Returns string with all stats, including bonus stats from armour
        builder.append(heroType+" details:");
        builder.append("\nHP: "+health);
        builder.append("\nStr: "+strength);
        builder.append("\nDex: "+dexterity);
        builder.append("\nInt: "+intelligence);
        builder.append("\nLvl: "+level);
        return builder.toString();
    }

}
