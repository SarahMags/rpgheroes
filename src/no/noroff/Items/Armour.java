package no.noroff.Items;

public class Armour {
    private final ArmourMaterial armourMaterial;
    private final ArmourSlot armourSlot;
    protected String name;
    protected int level;
    public int bonusHP;
    public int bonusStr;
    public int bonusInt;
    public int bonusDex;

    public Armour(ArmourMaterial armourMaterial, ArmourSlot armourSlot, String name, int level) {
        this.armourMaterial = armourMaterial;
        this.armourSlot = armourSlot;
        this.name = name;
        this.level = level;

        switch (armourMaterial){ //Sets the bonus stats and scaling depending on armour material. Bonus stats are added to hero base stats when armour is equipped
            case CLOTH:
                bonusHP = 10+(5*level);
                bonusStr = 0;
                bonusInt = 3+(2*level);
                bonusDex = 1+(level);
                break;
            case LEATHER:
                bonusHP = 20+(8*level);
                bonusStr = 1+(level);
                bonusInt = 0;
                bonusDex = 3+(2*level);
                break;
            case PLATE:
                bonusHP = 30+(12*level);
                bonusStr = 3+(2*level);
                bonusInt = 0;
                bonusDex = 1+(level);
                break;
        }

        switch (armourSlot){ //Scaling the bonusHP depending on armour slot. Body armour = 100%, Head = 80%, legs = 60%
            case BODY:
                //Body slot bonus stats stay at 100%
                break;
            case HEAD: //Head slot bonus stats converts to 80%
                bonusHP = (int)(bonusHP*0.8);
                bonusStr  = (int)(bonusStr*0.8);
                bonusInt  = (int)(bonusInt*0.8);
                bonusDex = (int)(bonusDex*0.8);
                break;
            case LEGS: //Leg slot bonus stats converts to 60%
                bonusHP = (int)(bonusHP*0.6);
                bonusStr  = (int)(bonusStr*0.6);
                bonusInt  = (int)(bonusInt*0.6);
                bonusDex = (int)(bonusDex*0.6);
                break;
        }
    }

    public String printArmour(){ //Returns a string containing info about the armour and its bonus stats.
        StringBuilder builder = new StringBuilder();
        builder.append("Item stats for: "+name);
        builder.append("\nArmour type: "+armourMaterial);
        builder.append("\nSlot: "+armourSlot);
        builder.append("\nArmour level: "+level);
        builder.append("\nBonus HP: "+bonusHP);
        builder.append("\nBonus Str: "+bonusStr);
        builder.append("\nBonus Int: "+bonusInt);
        builder.append("\nBonus Dex: "+bonusDex);
        return builder.toString();
    }

    public ArmourSlot getArmourSlot(){
        return armourSlot;
    }

    public int getArmourLevel(){
        return level;
    }
}
