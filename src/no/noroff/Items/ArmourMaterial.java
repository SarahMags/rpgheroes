package no.noroff.Items;

public enum ArmourMaterial {
    CLOTH,
    LEATHER,
    PLATE
}
