package no.noroff.Items;

public enum WeaponType {
    MELEE,
    RANGED,
    MAGIC
}
