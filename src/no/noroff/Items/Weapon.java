package no.noroff.Items;

public class Weapon {
    private final WeaponType weaponType;
    protected String name;
    protected int level;
    public int baseDmg;

    public Weapon(WeaponType weaponType, String name, int level) {
        this.weaponType = weaponType;
        this.name = name;
        this.level = level;


        switch (weaponType){ //setting weapon base damage and scaling depending on weapon type.
            case MELEE:
                baseDmg = 15+(2*level);
                break;
            case RANGED:
                baseDmg = 5+(3*level);
                break;
            case MAGIC:
                baseDmg = 25+(2*level);
                break;
        }
    }

    public WeaponType getWeaponType(){
        return weaponType;
    }

    public int getWeaponLevel(){
        return level;
    }

}
