package no.noroff.Items;
//Enums used in Armour.java to set bonus stats, and in Hero.java to equip the armour in the right armour slot variable
public enum ArmourSlot {
    HEAD,
    BODY,
    LEGS
}
