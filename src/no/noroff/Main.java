package no.noroff;

import no.noroff.Heroes.Hero;
import no.noroff.Heroes.HeroType;
import no.noroff.Items.*;

public class Main {

    public static void main(String[] args) {
    	//Creating three different type of warriors
		Hero warrior1 = new Hero(HeroType.WARRIOR);
		Hero mage1 = new Hero(HeroType.MAGE);
		Hero ranger1 = new Hero(HeroType.RANGER);

		//Creating melee, ranged and magic weapon
		Weapon axe = new Weapon(WeaponType.MELEE, "Axe of the Vikings", 3);
		Weapon bow = new Weapon(WeaponType.RANGED, "Bow of the Archers", 2);
		Weapon staff = new Weapon(WeaponType.MAGIC, "Staff of the Wizards", 3);

		//Creating head, body and legs armour
		Armour helmet = new Armour(ArmourMaterial.CLOTH, ArmourSlot.HEAD, "Cloth Helmet of the Farmer", 1);
		Armour helmet2 = new Armour(ArmourMaterial.LEATHER, ArmourSlot.HEAD, "Leather Helmet of the Butcher", 2);
		Armour chestPlate = new Armour(ArmourMaterial.PLATE, ArmourSlot.BODY, "Chest plate of the Knight", 3);
		Armour leggings = new Armour(ArmourMaterial.PLATE, ArmourSlot.LEGS, "Plate leggings of the Knight", 4);

		//Printing stats, giving heroes xp to level up, and printing stats again to show the increase
		System.out.println(mage1.printStats());
		System.out.println(mage1.gainXP(464));
		System.out.println(mage1.printStats());
		/*
		System.out.println(warrior1.printStats());
		System.out.println(warrior1.gainXP(1000));
		System.out.println(warrior1.printStats());
		*/

		//Equipping two different helmets, showing that the stats change. Lastly equipping chest plate, showing that stats increase with two equipped armour items
		/*System.out.println(warrior1.equipArmour(helmet));
		System.out.println(warrior1.printStats());
		System.out.println(warrior1.equipArmour(helmet2));
		System.out.println(warrior1.printStats());
		System.out.println(warrior1.equipArmour(chestPlate));
		System.out.println(warrior1.printStats());*/


		//Equipping different (same level) weapons, showing that the staff gives a higher attack power when equipped by mage
		/*System.out.println(mage1.equipWeapon(axe));
		System.out.println(mage1.printAttack());
		System.out.println(mage1.equipWeapon(staff));
		System.out.println(mage1.printAttack());*/
	}
}
